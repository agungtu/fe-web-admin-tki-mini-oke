# api utk get, upload : document pdf, doc, excel
# api utk post, get, update file

from fastapi import FastAPI, Request, Depends
from fastapi.security import OAuth2PasswordRequestForm
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse
from starlette.middleware.sessions import SessionMiddleware
import requests
from starlette.responses import RedirectResponse

from route.admin import router_admin

app = FastAPI(openapi_url="/web_admin/openapi.json",docs_url="/web_admin/swgr")
app.mount("/web_admin/static", StaticFiles(directory="static"), name="static")
app.mount("/assets", StaticFiles(directory="aset"), name="aset")


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)

app.add_middleware(SessionMiddleware, secret_key='ifpeoiu83iueoi9028')

@app.get("/health")
async def health():
    return {"status": "ok"}

templates = Jinja2Templates(directory="admin")

@app.post("/web_admin/login")
async def post_login(request: Request, form_data: OAuth2PasswordRequestForm = Depends()):
    url = "http://tki-backend-web-service/web_api/auth/login"
    # url = "http://127.0.0.1:8000/web_api/auth/login"
    dLogin = {"username":form_data.username,"password":form_data.password}
    res = requests.post(url,data=dLogin)
    response = res.json()
    print(response['companyId'])
    request.session['companyId'] = response['companyId']
    request.session['token'] = response["access_token"]
    return res.json()


app.include_router(router_admin,prefix="/web_admin",tags=["admin"],responses={404: {"description": "Not found"}})
app.mount("/web_admin/assets", StaticFiles(directory="aset"))
app.mount("/web_admin/karirnih/assets", StaticFiles(directory="aset"))