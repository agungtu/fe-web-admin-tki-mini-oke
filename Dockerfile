FROM 533939626955.dkr.ecr.ap-southeast-1.amazonaws.com/fastapi-erp:0.1

# Install requirements
COPY ./requirements.txt /app/
RUN pip3 install -r requirements.txt
RUN apt install -y nano git

# Copy source code
COPY . /app