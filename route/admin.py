# api utk get, upload : document pdf, doc, excel
# api utk post, get, update file

# from ast import For
from importlib.metadata import files
from os import error
import os
import shutil
from fastapi import APIRouter, Request, Depends,Form, File, UploadFile, Response
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse,RedirectResponse
import requests
import pandas as pd
import starlette.status as status
# import random, string
from pprint import pprint
from typing import List
from io import BytesIO

from config.setting import BASE_URL


# from fastapi import APIRouter, Request, Depends,Form
# from fastapi.security import OAuth2PasswordRequestForm
# from fastapi.templating import Jinja2Templates
# from fastapi.responses import HTMLResponse,RedirectResponse
# import requests
# import starlette.status as status

router_admin = APIRouter()

templates = Jinja2Templates(directory="./admin")

def uploadImage(file_name, image, token, id) :
    url2 = "http://tki-backend-web-service/web_api/web_gallery/gallery"
    datas2 = {"filename": file_name, "id":id}
    image.file.seek(0)
    tes = image.file.read()
    r = requests.post(url2, data=datas2, files={"image": tes}, headers={"Authorization" : "Bearer "+token})
    rs = r.json()
    return rs

@router_admin.get("/login", response_class=HTMLResponse)
async def get_login(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("index.html", {"request": request, "halaman":"home"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"Client Institusi"})
    

@router_admin.get("/signup", response_class=HTMLResponse)
async def get_signup(request: Request):
    return templates.TemplateResponse("signup.html", {"request": request, "tp":"admin", "tipe":"Client Institusi"})

@router_admin.get("/login", response_class=HTMLResponse)
async def get_login(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("index.html", {"request": request, "halaman":"home"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"Client Institusi"})

@router_admin.get("/home", response_class=HTMLResponse)
async def get_home(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("index.html", {"request": request, "halaman":"home"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"Client Institusi"})

@router_admin.get("/user", response_class=HTMLResponse)
async def get_leads(request: Request):
    token = request.session.get('token')
    if token is not None:
        return templates.TemplateResponse("index.html", {"request": request, "halaman":"user"})
    else:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"Client Institusi"})

@router_admin.get("/logout", response_class=HTMLResponse)
async def get_logout(request: Request):
    request.session['token'] = None
    return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"Client Institusi"})

  # TESTIMONY SHOW DATA
@router_admin.get("/testimony", response_class=HTMLResponse)
async def get_testimony(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_testimony/get_testimony/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"testimony", "data": response["content"]})

# ADD TESTIMONY

@router_admin.post("/testimony", response_class=HTMLResponse)
async def add_testimony(
        request: Request, 
        nama: str = Form(...),
        profesi: str = Form(...),
        title: str = Form(...),
        description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_testimony/testimony"
    dataa = {"nama":nama,"profesi":profesi, "title": title, "description": description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(token)
    # return res.json()
    # return str(response)
    return RedirectResponse("/web_admin/testimony", status_code=status.HTTP_302_FOUND)

#edit testimonny
@router_admin.post("/testimonyy", response_class=HTMLResponse)
async def edit_testimony(
        request: Request, 
        nama: str = Form(...),
        profesi: str = Form(...),
        title: str = Form(...),
        description: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_testimony/testimony/" + _id
    dataa = {"nama":nama,"profesi":profesi, "title": title, "description": description, "token": token}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/testimony", status_code=status.HTTP_302_FOUND)

#Delete testimony 
@router_admin.get("/testimony/delete/{id_}")
async def delete_testimony(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_testimony/testimony/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/testimony", status_code=status.HTTP_302_FOUND) 

#show data TUTORIAL
@router_admin.get("/tutorial", response_class=HTMLResponse)
async def get_tutorial(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_tutorial/get_tutorial/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"tutorial", "data": response["content"]})

#add data TUTORIAL
@router_admin.post("/tutorial", response_class=HTMLResponse)
async def add_tutorial(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        title: str = Form(...),
        description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_tutorial/tutorial"
    dataa = {"title":title,"description":description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/tutorial", status_code=status.HTTP_302_FOUND)
#edit TUTORIAL
@router_admin.post("/tutoriall", response_class=HTMLResponse)
async def edit_tutorial(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        title: str = Form(...),
        description: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_tutorial/tutorial/" + _id
    dataa = {"title": title, "description": description, "token": token}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/tutorial", status_code=status.HTTP_302_FOUND) 

#delete TUTORIAL
@router_admin.get("/tutorialll/delete/{id_}")
async def delete_tutorial(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_tutorial/tutorial/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/tutorial", status_code=status.HTTP_302_FOUND) 

#show data about
@router_admin.get("/about", response_class=HTMLResponse)
async def get_about(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_about/get_about/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"about", "data": response["content"]})

#add data about
@router_admin.post("/about", response_class=HTMLResponse)
async def add_about(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        title: str = Form(...),
        description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_about/about"
    dataa = {"title":title,"description":description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/about", status_code=status.HTTP_302_FOUND)

#edit about
@router_admin.post("/aboutt", response_class=HTMLResponse)
async def edit_about(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        title: str = Form(...),
        description: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_about/about/" + _id
    dataa = {"title": title, "description": description, "token": token}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/about", status_code=status.HTTP_302_FOUND) 

#delete about
@router_admin.get("/about/delete/{id_}")
async def delete_about(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_about/about/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/about", status_code=status.HTTP_302_FOUND) 

#show data News
@router_admin.get("/news", response_class=HTMLResponse)
async def get_news(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_news/get_news/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"news", "data": response["content"]})

# ADD news
@router_admin.post("/news", response_class=HTMLResponse)
async def add_news(
        request: Request, 
        title: str = Form(...),
        note: str = Form(...),
        description2: str = Form(...),
        description3: str = Form(...),
        description4: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_news/news"
    dataa = {"title":title, "description":description,"description2":description2,"description3":description3,"description4":description4,"note" : note, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/news", status_code=status.HTTP_302_FOUND)


 #edit News
@router_admin.post("/newsyuk", response_class=HTMLResponse)
async def edit_news(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_news/news/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/news", status_code=status.HTTP_302_FOUND)

@router_admin.get("/news/delete/{id_}")
async def delete_news(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_news/news/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/news", status_code=status.HTTP_302_FOUND)

#show data faq
@router_admin.get("/faq", response_class=HTMLResponse)
async def get_faq(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_faq/get_faq"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"faq", "data": response["content"]})

# ADD FAQ
@router_admin.post("/faq", response_class=HTMLResponse)
async def add_faq(
        request: Request, 
        question: str = Form(...),
        answer: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_faq/faq"
    dataa = {"question":question, "answer":answer, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/faq", status_code=status.HTTP_302_FOUND)

    #edit faq
@router_admin.post("/faqq", response_class=HTMLResponse)
async def edit_faqq(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        question: str = Form(...),
        answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_faq/faq/" + _id
    dataa = {"question": question, "answer": answer, "token": token}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/faq", status_code=status.HTTP_302_FOUND) 

#delete faq
@router_admin.get("/faq/delete/{id_}")
async def delete_about(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_faq/faq/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/faq", status_code=status.HTTP_302_FOUND)

#SHOW ACHIEVEMENT
@router_admin.get("/achievement", response_class=HTMLResponse)
async def get_achievement(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_achievement/get_achievement/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"achievement", "data": response["content"]})

# ADD ACHIEVEMENT
@router_admin.post("/achievement", response_class=HTMLResponse)
async def add_achievement(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_achievement/achievement"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/achievement", status_code=status.HTTP_302_FOUND)

    #edit ACHIEVEMENT
@router_admin.post("/achievementnih", response_class=HTMLResponse)
async def edit_achievementnih(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_achievement/achievement/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/achievement", status_code=status.HTTP_302_FOUND) 

    #delete ACHIEVEMENNT
@router_admin.get("/achievementnihh/delete/{id_}")
async def delete_achievementnihh(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_achievement/achievement/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/achievement", status_code=status.HTTP_302_FOUND)

#SHOW FEATURE
@router_admin.get("/feature", response_class=HTMLResponse)
async def get_feature(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_feature/get_feature/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"feature", "data": response["content"]})

# ADD FEATURE
@router_admin.post("/feature", response_class=HTMLResponse)
async def add_feature(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_feature/feature"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/feature", status_code=status.HTTP_302_FOUND)

 #edit FEATURE
@router_admin.post("/featuree", response_class=HTMLResponse)
async def edit_featuree(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_feature/feature/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/feature", status_code=status.HTTP_302_FOUND) 
  
  
#delete FEATURE
@router_admin.get("/feature/delete/{id_}")
async def delete_feature(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"feature", "tipe":"feature"})

    url = "http://tki-backend-web-service/web_api/web_feature/feature/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/feature", status_code=status.HTTP_302_FOUND)

#SHOW PRICING
@router_admin.get("/pricing", response_class=HTMLResponse)
async def get_pricing(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_pricing/get_pricing/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"pricing", "data": response["content"]})

  
# ADD pricing
@router_admin.post("/pricing", response_class=HTMLResponse)
async def add_pricing(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_pricing/pricing"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/pricing", status_code=status.HTTP_302_FOUND)

#edit FEATURE
@router_admin.post("/pricingg", response_class=HTMLResponse)
async def edit_pricingg(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_pricing/pricing/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/pricing", status_code=status.HTTP_302_FOUND) 
 
#delete PRICING
@router_admin.get("/pricing/delete/{id_}")
async def delete_news(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_pricing/pricing/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/pricing", status_code=status.HTTP_302_FOUND)

#SHOW PARTNER
@router_admin.get("/partner", response_class=HTMLResponse)
async def get_partner(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_partner/get_partner/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"partner", "data": response["content"]})

# ADD PARTNER
@router_admin.post("/partner", response_class=HTMLResponse)
async def add_partner(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_partner/partner"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/partner", status_code=status.HTTP_302_FOUND)

#edit PARTNER
@router_admin.post("/partnerr", response_class=HTMLResponse)
async def edit_partnerr(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_partner/partner/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/partner", status_code=status.HTTP_302_FOUND) 

#delete PARTNER
@router_admin.get("/partnerrr/delete/{id_}")
async def delete_partnerrr(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_partner/partner/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/partner", status_code=status.HTTP_302_FOUND)

#SHOW GALLERY
@router_admin.get("/gallery", response_class=HTMLResponse)
async def get_gallery(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_gallery/get_gallery/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"gallery", "data": response["content"]})

# ADD GALLERY
@router_admin.post("/gallery", response_class=HTMLResponse)
async def add_gallery(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        imageUrl: List[UploadFile] = File(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})
    file_name = []
    for x in imageUrl :
        changedName = x.filename.replace(" ", "_")
        fileAttr = changedName.split(".")
        tipe = fileAttr[-1]
        file_name.append(fileAttr[0]+"."+tipe)

    url = "http://tki-backend-web-service/web_api/web_gallery/gallery"
    dataa = {"title":title, "description":description, "imageUrl": imageUrl[0].filename, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    # for (x,y) in zip(file_name, imageUrl) :
    #     upup = uploadImage(response["_id"] , x, y, token, response["_id"])

    return RedirectResponse("/web_admin/gallery", status_code=status.HTTP_302_FOUND)
 

    #edit GALLERY
@router_admin.post("/galleryy", response_class=HTMLResponse)
async def edit_galleryy(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_gallery/gallery/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/gallery", status_code=status.HTTP_302_FOUND) 

#delete GALLERY
@router_admin.get("/galleryui/delete/{id_}")
async def delete_galleryui(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_gallery/gallery/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/gallery", status_code=status.HTTP_302_FOUND)

#SHOW VP
@router_admin.get("/videoproduct", response_class=HTMLResponse)
async def get_videoproduct(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_video_product/get_video_product/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"videoproduct", "data": response["content"]})

# ADD VP
@router_admin.post("/videoproduct", response_class=HTMLResponse)
async def add_videoproduct(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # description: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_video_product/video_product"
    dataa = {"title":title, "description":description,"token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/videoproduct", status_code=status.HTTP_302_FOUND)

 #edit VP
@router_admin.post("/videoproductt", response_class=HTMLResponse)
async def edit_videoproductt(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_video_product/video_product/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/videoproduct", status_code=status.HTTP_302_FOUND)

#delete VP
@router_admin.get("/videoproducttt/delete/{id_}")
async def delete_videoproducttt(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_video_product/video_product/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/videoproduct", status_code=status.HTTP_302_FOUND)

#SHOW wekkly
@router_admin.get("/weekly", response_class=HTMLResponse)
async def get_weekly(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_weekly/get_weekly/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    print(response["content"])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"weekly", "data": response["content"]})

# ADD weekly
@router_admin.post("/weekly", response_class=HTMLResponse)
async def add_weekly(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # imageUrl: str = Form(...),
        dateStart: str = Form(...),
        dateFinish: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_weekly/weekly"
    dataa = {"title":title, "description":description,"dateStart":dateStart, "dateFinish":dateFinish, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/weekly", status_code=status.HTTP_302_FOUND)

#edit WEEKLY
@router_admin.post("/weeklyy", response_class=HTMLResponse)
async def edit_weeklyy(
        request: Request, 
        title: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_weekly/weekly/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/weekly", status_code=status.HTTP_302_FOUND)

#delete weekly
@router_admin.get("/weeklyyy/delete/{id_}")
async def delete_weeklyyy(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_weekly/weekly/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/weekly", status_code=status.HTTP_302_FOUND)

#SHOW SLIDER
@router_admin.get("/slider", response_class=HTMLResponse)
async def get_slider(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = "http://tki-backend-web-service/web_api/web_slider/get_slider/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"slider", "data": response["content"]})

# ADD SLIDER
@router_admin.post("/slidera", response_class=HTMLResponse)
async def add_slider(
        request: Request, 
        title: str = Form(...),
        # tipe: str = Form(...),
        description: str = Form(...),
        # dateStart: str = Form(...),
        # dateFinish: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://tki-backend-web-service/web_api/web_slider/slider"
    dataa = {"title":title,  "description":description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/slider", status_code=status.HTTP_302_FOUND)

#edit SLIDER
@router_admin.post("/sliderr", response_class=HTMLResponse)
async def edit_sliderr(
        request: Request, 
        title: str = Form(...),
        # tipe: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://tki-backend-web-service/web_api/web_slider/slider/" + _id
    dataa = {"title": title, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/slider", status_code=status.HTTP_302_FOUND)

#delete slider
@router_admin.get("/sliderrr/delete/{id_}")
async def delete_sliderrr(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://tki-backend-web-service/web_api/web_slider/slider/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/slider", status_code=status.HTTP_302_FOUND)

    #SHOW PRODUCT
@router_admin.get("/product", response_class=HTMLResponse)
async def get_product(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://tki-backend-web-service/web_api/web_product/get_product/61a9d7a549cc3b14d5f9d356"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"product", "data": response["content"]})

# ADD PRODUCT
@router_admin.post("/product", response_class=HTMLResponse)
async def add_product(
        request: Request, 
        title: str = Form(...),
        tipe: str = Form(...),
        description: str = Form(...),
        # dateStart: str = Form(...),
        # dateFinish: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://tki-backend-web-service/web_api/web_product/product/"
    dataa = {"title":title, "tipe": tipe, "description":description, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/product", status_code=status.HTTP_302_FOUND)

#edit PRODUCT
@router_admin.post("/productt", response_class=HTMLResponse)
async def edit_productt(
        request: Request, 
        title: str = Form(...),
        tipe: str = Form(...),
        description: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://tki-backend-web-service/web_api/web_product/product/" + _id
    dataa = {"title": title, "tipe": tipe, "description": description}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()
    # return str(response)
    return RedirectResponse("/web_admin/product", status_code=status.HTTP_302_FOUND)

#delete product
@router_admin.get("/producttt/delete/{id_}")
async def delete_producttt(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://tki-backend-web-service/web_api/web_product/product/" + id_
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/product", status_code=status.HTTP_302_FOUND)

#SHOW KARIR
@router_admin.get("/karir", response_class=HTMLResponse)
async def get_karir(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    # url = "http://127.0.0.1:8000/web_api/web_karir/get_karir/61a9d7a549cc3b14d5f9d356"
    # url = "http://127.0.0.1:8000/web_api/web_karir/get_karir/620477d10f65723ddf9d0d3e"
    url = BASE_URL + "/web_api/web_karir/get_karir/61cc835d8acdf7b6052bf18c"
    # url="http://tki-backend-web-service/web_api/web_karir/get_karir/620477d10f65723ddf9d0d3e"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa)
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"karir", "data": response["content"]})

# ADD KARIR
@router_admin.post("/karir", response_class=HTMLResponse)
async def add_karir(
        request: Request, 
        position: str = Form(...),
        statuss: str = Form(...),
        note: str = Form(...),
        deadline: str = Form(...),
        # imageUrl: str = Form(...),
        # dateStart: str = Form(...),
        # dateFinish: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    # url = "http://tki-backend-web-service/web_api/web_karir/karir"
    # url = "http://127.0.0.1:8000/web_api/web_karir/karir"
    url = BASE_URL + "/web_api/web_karir/karir"
    dataa = {"position": position, "status" : statuss, "note" : note, "deadline" : deadline, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/karir", status_code=status.HTTP_302_FOUND)

#edit KARIR
@router_admin.post("/karirr", response_class=HTMLResponse)
async def edit_karirr(
        request: Request, 
        position: str = Form(...),
        statuss: str = Form(...),
        note: str = Form(...),
        deadline: str = Form(...),
        # text2: str = Form(...),
        # text3: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    url = BASE_URL + "/web_api/web_karir/karir/" + _id
    dataa = {"position": position, "status": statuss, "note" : note, "deadline" : deadline}   
    # print(dataaa)
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/karir", status_code=status.HTTP_302_FOUND)

#delete karir
@router_admin.get("/karir/delete/{id_}")
async def delete_karir(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        id_: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    # url = "http://tki-backend-web-service/web_api/web_karir/karir/" + id_
    url = BASE_URL + "/web_api/web_karir/karir/" + id_

    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/karir", status_code=status.HTTP_302_FOUND)

 #SHOW PELAMAR
@router_admin.get("/karirnih/{id}", response_class=HTMLResponse)
async def get_pelamar(request: Request, id : str):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://tki-backend-web-service/web_api/web_karir/karir/pelamar/get_pelamar/" + id
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.get(url, json=dataa, headers={"Authorization" : "Bearer " + token}) 
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    if "detail" in response : 
        return templates.TemplateResponse("index.html", {"request": request, "halaman":"pelamar", "data":[]})
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"pelamar", "data": response["pelamar"]})

# ADD PELAMAR
@router_admin.post("/pelamar", response_class=HTMLResponse)
async def add_pelamar(
        request: Request, 
        # requirement: str = Form(...),
        firstName: str = Form(...),
        lastName: str = Form(...),
        email: str = Form(...),
        gender: str = Form(...),
        lastEducation: str = Form(...),

        # dateStart: str = Form(...),
        # dateFinish: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://tki-backend-web-service/web_api/web_karir/karir/pelamar/61c02184793938c238cfaeaf"
    dataa = {"firstName": firstName, "lastName" : lastName, "email" : email, "gender": gender,"lastEducation": lastEducation, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # return res.json()
    print(response)
    # return str(response)

    return RedirectResponse("/web_admin/pelamar", status_code=status.HTTP_302_FOUND)

#delete PELAMAR
@router_admin.get("/pelamarrr/delete/{idPelamar}")
async def delete_pelamarrr(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        idPelamar: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://tki-backend-web-service/web_api/web_karir/karir/pelamar/61bc3729d1fe18262d73ed4d/" + idPelamar
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/pelamar", status_code=status.HTTP_302_FOUND)

#edit PELAMAR
@router_admin.post("/pelamarr", response_class=HTMLResponse)
async def edit_pelamar(
        request: Request, 
        firstName: str = Form(...),
        lastName: str = Form(...),
        email: str = Form(...),
        gender: str = Form(...),
        lastEducation: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://tki-backend-web-service/web_api/web_karir/karir/pelamar/61c02184793938c238cfaeaf/" + _id
    dataa = {"firstName": firstName, "lastName" : lastName, "email" : email, "gender": gender,"lastEducation": lastEducation, "token": token}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()
    # return str(response)http://tki-backend-web-service/web_api/web_karir/karir/pelamar/61c02184793938c238cfaeaf/
    return RedirectResponse("/web_admin/pelamar", status_code=status.HTTP_302_FOUND)

#SHOW LIST DETAIL
@router_admin.get("/listdetail", response_class=HTMLResponse)
async def get_listdetail(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"partner"})

    url = "http://tki-backend-web-service/web_api/web_listdetail/get_list/about/61bbf1b85d1e2bc927b996ec"
    dataa = {"Authorization" :"Bearer" + token}
    res = requests.get(url, json=dataa, headers={"Authorization" : "Bearer " + token}) 
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"listdetail", "data": response})

# ADD LISTDETAIL
@router_admin.post("/listdetail", response_class=HTMLResponse)
async def add_listdetail(
        request: Request, 
        title: str = Form(...),
        text1: str = Form(...),
        text2: str = Form(...),
        text3: str = Form(...),
        # dateStart: str = Form(...),
        # dateFinish: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://tki-backend-web-service/web_api/web_listdetail/list/about/61bbf1b85d1e2bc927b996ec"
    dataa = {"title": title, "text1" : text1, "text2" : text2, "text3" : text3, "token": token}
    # print(dataaa)
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/listdetail", status_code=status.HTTP_302_FOUND)

#edit LISTDETAIL
@router_admin.post("/listdetaill", response_class=HTMLResponse)
async def edit_listdetaill(
        request: Request, 
        title: str = Form(...),
        text1: str = Form(...),
        text2: str = Form(...),
        text3: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        listId: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://tki-backend-web-service/web_api/web_listdetail/list/about/61bbf1b85d1e2bc927b996ec/" + listId
    dataa = {"title": title, "text1": text1, "text2": text2, "text3": text3}
    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/listdetail", status_code=status.HTTP_302_FOUND)

#delete listdetail
@router_admin.get("/listdetail/delete/{list_id}")
async def delete_listdetail(
        request: Request, 
        # nama: str = Form(...),
        # profesi: str = Form(...),
        # title: str = Form(...),
        # description: str = Form(...),
        list_id: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request,})

    url = "http://tki-backend-web-service/web_api/web_listdetail/list/about/61bbf1b85d1e2bc927b996ec/" + list_id  
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()

    # return str(response)

    return RedirectResponse("/web_admin/listdetail", status_code=status.HTTP_302_FOUND)


#SHOW WEBINAR
@router_admin.get("/webinar", response_class=HTMLResponse)
async def get_webinar(request: Request, error : str = None):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    # url = "http://tki-backend-web-service/web_api/web_webinar/get_webinar/61a9d7a549cc3b14d5f9d356" 
    url = "http://tki-backend-web-service/web_api/web_webinar/get_webinar"
    # url = "http://127.0.0.1:8000/web_api/web_webinar/get_webinar"

    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token}) 
    response = res.json()
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"webinar", "data": response["content"], "error": error})

#SHOW PESERTA WEBINAR
@router_admin.get("/peserta_webinar", response_class=HTMLResponse)
async def get_pesertawebinar(request: Request):
    token = request.session.get('token')
    # print(request.session)
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"admin", "tipe":"admin"})

    # url = "http://tki-backend-web-service/web_api/web_webinar/get_webinar/61a9d7a549cc3b14d5f9d356" 
    url = "http://tki-backend-web-service/web_api/web_webinar/get_peserta_webinar"
    # url   = "http://127.0.0.1:8000/web_api/web_webinar/get_peserta_webinar"

    dataa = {"Authorization" :"Bearer" + token}
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token}) 
    response = res.json()
    # pprint(response["content"])
    # print(response["content"][0])
    # request.session['token'] = response["access_token"]
    # return res.json()
    # print(response)
    return templates.TemplateResponse("index.html", {"request": request, "halaman":"peserta_webinar", "data": response['content']})

#report by id webinar
@router_admin.post("/webinar/report_peserta_by_idWebinar.xlsx", response_class=HTMLResponse)
async def report_kategori(
        request: Request,
        idwebinar : str = Form(...),
    ):
    token = request.session.get('token')
    role = request.session.get('role')
    # print(role)
    
    url = "http://tki-backend-web-service/web_api/web_webinar/peserta_webinar/report/" + idwebinar
    # url = "http://127.0.0.1:8000/web_api/web_webinar/peserta_webinar/report/" + idwebinar

    datas = {"token" : token}
    res = requests.post(url, json=datas, headers={"Authorization" : "Bearer "+token})
    response = res.json()
    # print(url)
    datas = response["content"]
    pprint(datas)
    excel = {
        "Nama": [],
        "Usia": [],
        "Email": [],
        "Whatsapp": [],
        "Kota": [],
        "Provinsi":[],
        "Facebook": [],
        "Instagram": [],
        "Websites": [],
        "Institusi": [],
        "Jabatan": [],
        "jenisInstitusi":[],
        "Mengetahui": [],
        "pertanyaanProduk":[],
        "produkTki":[],
    }
    for x in datas:
        excel["Nama"].append(x['peserta']["nama"])
        excel["Usia"].append(x['peserta']["usia"])
        excel["Email"].append(x['peserta']["email"])
        excel["Whatsapp"].append(x['peserta']["whatsapp"])
        excel["Kota"].append(x['peserta']["kota"])
        excel["Provinsi"].append(x['peserta']["provinsi"])
        excel["Facebook"].append(x['peserta']["facebook"])
        excel["Instagram"].append(x['peserta']["instagram"])
        excel["Websites"].append(x['peserta']["websites"])
        excel["Institusi"].append(x['peserta']["institusi"])
        excel["Jabatan"].append(x['peserta']["jabatan"])
        excel["jenisInstitusi"].append(x['peserta']["jenisInstitusi"])
        excel["Mengetahui"].append(x['peserta']["mengetahui"])
        excel["pertanyaanProduk"].append(x['peserta']["pertanyaanProduk"])
        excel["produkTki"].append(x['peserta']["produkTki"])



    print(excel)
    df = pd.DataFrame(excel, columns = ['Nama', 'Usia', 'Email', "Whatsapp", 'Kota','Provinsi', 'Facebook','Instagram','Websites','Institusi','Jabatan', 'jenisInstitusi', 'Mengetahui','pertanyaanProduk','produkTki'])
    print(df)
    with BytesIO() as b:
        writer = pd.ExcelWriter(b, engine="xlsxwriter")
        df.to_excel(writer, sheet_name=f"report riwayat webinar by id")
        writer.save()
        return Response(content=b.getvalue(), media_type="application/vnd.ms-excel")

#report aset Bulan Riwayat
@router_admin.post("/aset/report/report_riwayat_aset_perbulan.xlsx", response_class=HTMLResponse)
async def bulan(
        request: Request,
        month : str = Form(...),
    ):
    token = request.session.get('token')
    role = request.session.get('role')
    
    url = "http://tki-backend-web-service/web_api/web_webinar/peserta_webinar/report/bulan/" + month
    
    datas = {"token" : token}
    res = requests.post(url, json=datas, headers={"Authorization" : "Bearer "+token})
    response = res.json()
    # print(url)
    datas = response["content"]
    pprint(datas)
    excel = {
        "Nama": [],
        "Usia": [],
        "Email": [],
        "Whatsapp": [],
        "Kota": [],
        "Facebook": [],
        "Instagram": [],
        "Websites": [],
        "Institusi": [],
        "Jabatan": [],
        "Jenis Institusi": [],
    }
    for x in datas:
        excel["Nama"].append(x['peserta']["nama"])
        excel["Usia"].append(x['peserta']["usia"])
        excel["Email"].append(x['peserta']["email"])
        excel["Whatsapp"].append(x['peserta']["whatsapp"])
        excel["Kota"].append(x['peserta']["kota"])
        excel["Facebook"].append(x['peserta']["facebook"])
        excel["Instagram"].append(x['peserta']["instagram"])
        excel["Websites"].append(x['peserta']["websites"])
        excel["Institusi"].append(x['peserta']["institusi"])
        excel["Jabatan"].append(x['peserta']["jabatan"])
        excel["Jenis Institusi"].append(x['peserta']["jenis_institusi"])

    print(excel)
    df = pd.DataFrame(excel, columns = ['Nama', 'Usia', 'Email', "Whatsapp", 'Kota', 'Facebook','Instagram','Websites','Institusi','Jabatan', 'Jenis institusi'])
    print(df)
    with BytesIO() as b:
        writer = pd.ExcelWriter(b, engine="xlsxwriter")
        df.to_excel(writer, sheet_name=f"report riwayat webinar by id")
        writer.save()
        return Response(content=b.getvalue(), media_type="application/vnd.ms-excel")


# ADD WEBINAR
@router_admin.post("/webinar", response_class=HTMLResponse)
async def add_webinar(
        request: Request, 
        # requirement: str = Form(...),
        gambar: UploadFile = File(...),
        judul: str = Form(...),
        description: str = Form(...),
        tanggal: str = Form(...),
        jam: str = Form(...),
        urlWebinar: str = Form(...),
        urlYoutube:str  = Form(...),
        
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    # folder = "upload_image"
    # originalFile = os.path.join(folder, gambar.filename)
    ## MENYIMPAN GAMBAR / FILE DI LOKAL
    with open(gambar.filename, "wb") as imgBuffer: #ini membuat file kosong binary
        shutil.copyfileobj(gambar.file, imgBuffer) #ini mengcopy object kedalam data binary

    size = os.path.getsize(gambar.filename) #ini mengambil ukuran file di lokal

    print(size)
    if size > 5000000:
        os.remove(gambar.filename) #menghapus file dilokal
        text = "File tidak boleh lebih dari 4MB"
        return RedirectResponse("/web_admin/webinar?error="+ text, status_code=status.HTTP_302_FOUND)


    url_gambar = "http://tki-backend-web-service/web_api/web_webinar/webinar/upload_gambar"
    # url_gambar = "http://127.0.0.1:8000/web_api/web_webinar/webinar/upload_gambar"

    # img = Image.open(gambar.filename)
    print(gambar.file)
    res = requests.post(url_gambar, files={"gambar": open(gambar.filename, 'rb')}, headers={"Authorization" : "Bearer " + token})
    # response = res.json()
    os.remove(gambar.filename)

    url = "http://tki-backend-web-service/web_api/web_webinar/webinar"
    # url = "http://127.0.0.1:8000/web_api/web_webinar/webinar"
    dataa = {
                "judul": judul, 
                "description" : description,
                "tanggal" : tanggal, 
                "jam" : jam,
                "urlWebinar" : urlWebinar,
                "urlYoutube" : urlYoutube,
                "image": [{
                    "filename": str(gambar.filename),
                    "contentType": str(gambar.content_type)
                }]
            }
    res = requests.post(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # return res.json()
    # print(response)
    # return str(response)
    return RedirectResponse("/web_admin/webinar", status_code=status.HTTP_302_FOUND)

#delete webinar
@router_admin.get("/webinaroi/delete/{id}")
async def delete_webinar(
        request: Request,
        id: str
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request,})

    url = "http://tki-backend-web-service/web_api/web_webinar/webinar/" + id
    # url = "http://127.0.0.1:8000/web_api/web_webinar/webinar/" + id
    dataa = {"token": token}
    # print(dataaa)
    
    res = requests.delete(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()
    # return str(response)
    return RedirectResponse("/web_admin/webinar", status_code=status.HTTP_302_FOUND)

#edit WEBINAR
@router_admin.post("/webinarr", response_class=HTMLResponse)
async def edit_webinarr(
        request: Request, 
        judul: str = Form(...),
        description: str = Form(...),
        tanggal: str = Form(...),
        jam: str = Form(...),
        urlWebinar: str = Form(...),
        urlYoutube: str = Form(...),
        # tags: str = Form(...),
        # answer: str = Form(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://tki-backend-web-service/web_api/web_webinar/webinar/" + _id
    # url = "http://127.0.0.1:8000/web_api/web_webinar/webinar/" + _id
    dataa = {"judul": judul, "description" : description,"tanggal" : tanggal, "jam" : jam,"urlWebinar" : urlWebinar,"urlYoutube" : urlYoutube, "token": token}    # print(dataaa)
    
    res = requests.put(url, json=dataa, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    # print(response)
    # return res.json()
    # return str(response)
    return RedirectResponse("/web_admin/webinar", status_code=status.HTTP_302_FOUND)


#edit WEBINAR Gambar
@router_admin.post("/webinarr/gambar", response_class=HTMLResponse)
async def edit_webinarr(
        request: Request, 
        gambar_edit: UploadFile = File(...),
        _id: str = Form(...),
    ):
    token = request.session.get('token')
    if token is None:
        return templates.TemplateResponse("login.html", {"request": request, "tp":"partner", "tipe":"Partner"})

    url = "http://tki-backend-web-service/web_api/web_webinar/webinar/upload_gambar/edit/" + _id
    # url = "http://127.0.0.1:8000/web_api/web_webinar/webinar/upload_gambar/edit/" + _id

    
    print(url)
    res = requests.post(url, files={"gambar": (gambar_edit.filename, gambar_edit.file, gambar_edit.content_type) }, headers={"Authorization" : "Bearer " + token})
    response = res.json()
    
    # print(response)
    # return res.json()
    # return str(response)
    return RedirectResponse("/web_admin/webinar", status_code=status.HTTP_302_FOUND)
